import React from 'react';


const Header = () => {
    
    return ( 
        <header>
            <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
                <p>lorem</p>
                
                <div
                    id="accessconfig" 
                    data-accessconfig-buttonname="Paramètres d’accessibilité" 
                    data-accessconfig-params='{ "Prefix" : "a42-ac", "Font" : false, "LineSpacing": false, "Justification": false, "ImageReplacement": false}'>
                </div>
            </div>
            </nav>
        </header>
     );
}
 
export default Header;