import React, {useRef, useEffect} from 'react';

const Header = () => {
    const ref = useRef(null);

    useEffect(() => {
        ref.current.dataset.accessconfigButtonname = "Paramètres d’accessibilité";
        ref.current.dataset.accessconfigParams = '{ "Prefix" : "a42-ac", "Font" : true, "LineSpacing": true, "Justification": true, "ImageReplacement": false}';
        document.body.innerHTML += '<script src="../accessconfig/js/accessconfig.min.js" type="text/javascript"></script>'
    }, [])

    

    return ( 
        <header>
            <nav className="navbar navbar-light bg-light">

            <div className="container-fluid">
                <p>lorem</p>
                <div ref={ref} id="accessconfig"></div>
            </div>

            </nav>
        </header>
     );
}
 
export default Header;