import React, { lazy, Suspense } from 'react'
import { BrowserRouter as Router, withRouter, Switch, Route } from "react-router-dom";

import ThreeDots from './ThreeDots'
const Main = lazy(() => import("./Main"));
const Test = lazy(() => import("./Test"));
const Header = lazy(() => import("./Header"));

const Rooter = () => {
   
    // Header
    const NavbarWithRouter = withRouter(Header);
    

    return (
        <Suspense fallback={<ThreeDots/>}>    
            <Router>
                <NavbarWithRouter/>
                <Switch>
                    <Route exact path="/" component={Main} />
                    <Route exact path="/access" component={Test} />
                </Switch>
            </Router>   
        </Suspense>          
    )
};

export default Rooter